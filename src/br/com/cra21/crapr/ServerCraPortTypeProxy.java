package br.com.cra21.crapr;

public class ServerCraPortTypeProxy implements br.com.cra21.crapr.ServerCraPortType {
  private String _endpoint = null;
  private br.com.cra21.crapr.ServerCraPortType serverCraPortType = null;
  
  public ServerCraPortTypeProxy() {
    _initServerCraPortTypeProxy();
  }
  
  public ServerCraPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initServerCraPortTypeProxy();
  }
  
  private void _initServerCraPortTypeProxy() {
    try {
      serverCraPortType = (new br.com.cra21.crapr.ServerCraLocator()).getServerCraPort();
      if (serverCraPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)serverCraPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)serverCraPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (serverCraPortType != null)
      ((javax.xml.rpc.Stub)serverCraPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.com.cra21.crapr.ServerCraPortType getServerCraPortType() {
    if (serverCraPortType == null)
      _initServerCraPortTypeProxy();
    return serverCraPortType;
  }
  
  public java.lang.String remessa(java.lang.String userArq) throws java.rmi.RemoteException{
    if (serverCraPortType == null)
      _initServerCraPortTypeProxy();
    return serverCraPortType.remessa(userArq);
  }
  
  public java.lang.String confirmacao(java.lang.String userArq, java.lang.String userDados) throws java.rmi.RemoteException{
    if (serverCraPortType == null)
      _initServerCraPortTypeProxy();
    return serverCraPortType.confirmacao(userArq, userDados);
  }
  
  public java.lang.String retorno(java.lang.String userArq, java.lang.String userDados) throws java.rmi.RemoteException{
    if (serverCraPortType == null)
      _initServerCraPortTypeProxy();
    return serverCraPortType.retorno(userArq, userDados);
  }
  
  public java.lang.String desistencia(java.lang.String userArq) throws java.rmi.RemoteException{
    if (serverCraPortType == null)
      _initServerCraPortTypeProxy();
    return serverCraPortType.desistencia(userArq);
  }
  
  public java.lang.String cancelamento(java.lang.String userArq) throws java.rmi.RemoteException{
    if (serverCraPortType == null)
      _initServerCraPortTypeProxy();
    return serverCraPortType.cancelamento(userArq);
  }
  
  public java.lang.String buscarApresentante() throws java.rmi.RemoteException{
    if (serverCraPortType == null)
      _initServerCraPortTypeProxy();
    return serverCraPortType.buscarApresentante();
  }
  
  
}