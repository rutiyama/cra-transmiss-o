package br.com.softcenter.cra;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import br.com.softcenter.cra.controller.RunScheduler;

public class MainScheduler {
	
	public MainScheduler() throws NoSuchAlgorithmException, UnsupportedEncodingException, SchedulerException{
		
//		ServerSocketConnection.getSocketConn().start();
		
		Logger.getLogger("br.com.softcenter.cra").info("Iniciando Serviço");
		Scheduler agendador = StdSchedulerFactory.getDefaultScheduler();
//		JobDetail job9 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa9", "grupo1").build();
//		CronTrigger trigger9 = TriggerBuilder.newTrigger().withIdentity("trigger9", "grupo1").
//				withSchedule(CronScheduleBuilder.cronSchedule("0/20 * * * * ?")).build();
		
		JobDetail job1 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa1", "grupo1").build();
		CronTrigger trigger1 = TriggerBuilder.newTrigger().withIdentity("trigger1", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 0 8 ? * MON-FRI *")).build();
		
		JobDetail job2 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa2", "grupo1").build();
		CronTrigger trigger2 = TriggerBuilder.newTrigger().withIdentity("trigger2", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 15 8 ? * MON-FRI *")).build();

		JobDetail job3 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa3", "grupo1").build();
		CronTrigger trigger3 = TriggerBuilder.newTrigger().withIdentity("trigger3", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 30 8 ? * MON-FRI *")).build();

		JobDetail job4 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa4", "grupo1").build();
		CronTrigger trigger4 = TriggerBuilder.newTrigger().withIdentity("trigger4", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 45 8 ? * MON-FRI *")).build();
		
		JobDetail job5 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa5", "grupo1").build();
		CronTrigger trigger5 = TriggerBuilder.newTrigger().withIdentity("trigger5", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 00 9 ? * MON-FRI *")).build();
		
		JobDetail job6 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa6", "grupo1").build();
		CronTrigger trigger6 = TriggerBuilder.newTrigger().withIdentity("trigger6", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 15 9 ? * MON-FRI *")).build();
		
		JobDetail job7 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa7", "grupo1").build();
		CronTrigger trigger7 = TriggerBuilder.newTrigger().withIdentity("trigger7", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 30 9 ? * MON-FRI *")).build();
		
		JobDetail job8 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa8", "grupo1").build();
		CronTrigger trigger8 = TriggerBuilder.newTrigger().withIdentity("trigger8", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 45 9 ? * MON-FRI *")).build();
		
		JobDetail job9 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa9", "grupo1").build();
		CronTrigger trigger9 = TriggerBuilder.newTrigger().withIdentity("trigger9", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 00 10 ? * MON-FRI *")).build();
		
		JobDetail job10 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa10", "grupo1").build();
		CronTrigger trigger10 = TriggerBuilder.newTrigger().withIdentity("trigger10", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 15 10 ? * MON-FRI *")).build();
		
		JobDetail job11 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa11", "grupo1").build();
		CronTrigger trigger11 = TriggerBuilder.newTrigger().withIdentity("trigger11", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 30 10 ? * MON-FRI *")).build();
		
		JobDetail job12 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa12", "grupo1").build();
		CronTrigger trigger12 = TriggerBuilder.newTrigger().withIdentity("trigger12", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 45 10 ? * MON-FRI *")).build();
		
		JobDetail job13 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa13", "grupo1").build();
		CronTrigger trigger13 = TriggerBuilder.newTrigger().withIdentity("trigger13", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 00 11 ? * MON-FRI *")).build();
		
		JobDetail job14 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa14", "grupo1").build();
		CronTrigger trigger14 = TriggerBuilder.newTrigger().withIdentity("trigger14", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 15 11 ? * MON-FRI *")).build();
		
		JobDetail job15 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa15", "grupo1").build();
		CronTrigger trigger15 = TriggerBuilder.newTrigger().withIdentity("trigger15", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 30 11 ? * MON-FRI *")).build();
		
		JobDetail job16 = JobBuilder.newJob(RunScheduler.class).withIdentity("tarefa16", "grupo1").build();
		CronTrigger trigger16 = TriggerBuilder.newTrigger().withIdentity("trigger16", "grupo1").
				withSchedule(CronScheduleBuilder.cronSchedule("0 45 11 ? * MON-FRI *")).build();
		
		agendador.scheduleJob(job1, trigger1);
		agendador.scheduleJob(job2, trigger2);
		agendador.scheduleJob(job3, trigger3);
		agendador.scheduleJob(job4, trigger4);		
		agendador.scheduleJob(job5, trigger5);
		agendador.scheduleJob(job6, trigger6);
		agendador.scheduleJob(job7, trigger7);
		agendador.scheduleJob(job8, trigger8);
		agendador.scheduleJob(job9, trigger9);
		agendador.scheduleJob(job10, trigger10);
		agendador.scheduleJob(job11, trigger11);
		agendador.scheduleJob(job12, trigger12);
		agendador.scheduleJob(job13, trigger13);
		agendador.scheduleJob(job14, trigger14);
		agendador.scheduleJob(job15, trigger15);
		agendador.scheduleJob(job16, trigger16);
		agendador.start();
	}
	
	public static void main (String args[]) throws NoSuchAlgorithmException, UnsupportedEncodingException, SchedulerException{
		new MainScheduler();
	}
}
