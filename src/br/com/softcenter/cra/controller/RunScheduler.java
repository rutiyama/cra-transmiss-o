package br.com.softcenter.cra.controller;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import br.com.softcenter.cra.controller.enums.ConsultarServico;
import br.com.softcenter.cra.controller.servicosdisponiveis.ServicoFactory;
import br.com.softcenter.cra.controller.servicosdisponiveis.Servicos;
import br.com.softcenter.cra.model.beans.BeanComarcasWS;
import br.com.softcenter.cra.model.dao.impl.ComarcasWSDAOImpl;
import br.com.softcenter.cra.model.dao.impl.DAOModel;
import br.com.softcenter.cra.utils.xml.CRAException;
import br.com.softcenter.cra.utils.xml.Remessa.XMLLogRemessa;

/**
 * @author Rodrigo Junior Utiyama
 * @since 01-09-2015
 */

public class RunScheduler implements Job {
	
	private Logger logger;
	
	@Override
	public void execute(JobExecutionContext context)  {
		this.logger = Logger.getLogger("br.com.softcenter.cra.controller");
		
		/* Se for a tarefa1, indica que é a primeira execução do dia. Portanto, 
		 * cria um novo arquivo de log para ser armazenado as informações daquele dia:
		 */
		if (context.getTrigger().getJobKey().getName().equals("tarefa1")){
			try{ 
				XMLLogRemessa.CriarArquivoLOG();
			}catch(IOException e){
				logger.error(e.getMessage());
			}
		}
		
		//Será verificado, todos os dias, cada uma das comarcas:
		for(BeanComarcasWS comarca : retornarComarcas()){
			iniciarVerificacao(comarca, ConsultarServico.REMESSA);
		}
	} 
	
	private synchronized void iniciarVerificacao(BeanComarcasWS comarca, ConsultarServico tipoDoServico){	
		/*
		 * Atualmente, passando o tipo de serviço como REMESSA. Instanciará um objeto
		 * do tipo ServicoRemessa (padrão factory):
		 */
		Servicos<Object, Object> servico = ServicoFactory.getInstance(tipoDoServico); 
		
		try{
			servico.validarServico(comarca);
		}catch(CRAException rem){
//			ServerSocketConnection.getSocketConn().sendToAll(rem.getMessage());
			logger.error(rem.getMessage());
		}catch(Exception e){
			logger.error(e.getMessage());
		}
	}
	
	private List<BeanComarcasWS> retornarComarcas(){
		DAOModel<BeanComarcasWS, Integer> comarcasDAO = new ComarcasWSDAOImpl();
		return comarcasDAO.select();
		
//		List<BeanComarcasWS> list = new ArrayList<BeanComarcasWS>();
//		list.add(new BeanComarcasWS("1", "01distpontagrossa","pontagrossa", "/home/rodrigo/Documentos/ponta grossa/"));
//		list.add(new BeanComarcasWS("2", "distcampolargo","01distcampolargo", "/home/rodrigo/Documentos/campo largo/"));
//		return list;
	}
	
}
