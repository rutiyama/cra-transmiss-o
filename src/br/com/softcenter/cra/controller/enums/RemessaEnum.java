package br.com.softcenter.cra.controller.enums;

import java.io.Serializable;

public enum RemessaEnum implements Serializable {
	FALHA_AUTENTICACAO(1, "Falha na autenticação."), 
	INSTITUICAO_INATIVA(9, "A instituição do usuário está inativa."),
	NOME_ARQUIVO_INVALIDO(2, "Nome do arquivo inválido."),
	REMESSA_INEXISTENTE(3, "Não existe remessa na data informada."),
	ERRO_GERAL(-1, "Erro desconhecido"),
	SUCESSO(200, "Sucesso");
	
	private int codigo;
	private String descricao;
	
	private RemessaEnum(int codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public String descricaoDoErro(){
		return this.descricao;
	}
	
	public int codigoDoErro(){
		return this.codigo;
	}
}
