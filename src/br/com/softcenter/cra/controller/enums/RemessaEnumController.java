package br.com.softcenter.cra.controller.enums;

public class RemessaEnumController {
	public static RemessaEnum retornarOcorrencia(int codigo){
		if (codigo == RemessaEnum.SUCESSO.codigoDoErro())
			return RemessaEnum.SUCESSO;
		else if(codigo == RemessaEnum.FALHA_AUTENTICACAO.codigoDoErro()) 
			return RemessaEnum.FALHA_AUTENTICACAO;
		else if (codigo == RemessaEnum.INSTITUICAO_INATIVA.codigoDoErro())
			return RemessaEnum.INSTITUICAO_INATIVA;
		else if (codigo == RemessaEnum.NOME_ARQUIVO_INVALIDO.codigoDoErro()) 
			return RemessaEnum.NOME_ARQUIVO_INVALIDO;
		else if (codigo == RemessaEnum.REMESSA_INEXISTENTE.codigoDoErro())
			return RemessaEnum.REMESSA_INEXISTENTE;
		else
			return RemessaEnum.ERRO_GERAL;
	}
}
