package br.com.softcenter.cra.controller.servicosdisponiveis;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.axis.AxisFault;
import org.xml.sax.SAXException;

import br.com.softcenter.cra.model.beans.BeanComarcasWS;
import br.com.softcenter.cra.utils.xml.CRAException;

public interface Servicos<T, t> {
	public t verificar(String conteudoarquivo) throws MalformedURLException, AxisFault, RemoteException,
			ParserConfigurationException, SAXException, IOException, Exception;

	public String retornarConteudo(String nomeArquivo, String login, String senha)
			throws MalformedURLException, AxisFault, RemoteException;

	public void exportar(BeanComarcasWS comarca)  throws CRAException, Exception;

	public String montarNomeGenericoSomenteParaConsulta();

	public void validarServico(T beanPrincipal)
			throws ParserConfigurationException, SAXException, IOException, Exception;
}
