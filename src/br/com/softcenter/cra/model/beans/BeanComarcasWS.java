package br.com.softcenter.cra.model.beans;

import java.io.Serializable;

public class BeanComarcasWS implements Serializable{
	private String codigoMunicipio;
	private String login;
	private String senha;
	private String pathRemessa;
	
	public BeanComarcasWS() {
	}

	public BeanComarcasWS(String codigoMunicipio, String login, String senha, String pathRemessa) {
		super();
		this.codigoMunicipio = codigoMunicipio;
		this.login = login;
		this.senha = senha;
		this.pathRemessa = pathRemessa;
	}

	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}

	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getPathRemessa() {
		return pathRemessa;
	}

	public void setPathRemessa(String pathRemessa) {
		this.pathRemessa = pathRemessa;
	}

	@Override
	public String toString() {
		return "BeanComarcasWS [codigoMunicipio=" + codigoMunicipio + ", login=" + login + ", senha=" + senha
				+ ", pathRemessa=" + pathRemessa + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoMunicipio == null) ? 0 : codigoMunicipio.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((pathRemessa == null) ? 0 : pathRemessa.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BeanComarcasWS other = (BeanComarcasWS) obj;
		if (codigoMunicipio == null) {
			if (other.codigoMunicipio != null)
				return false;
		} else if (!codigoMunicipio.equals(other.codigoMunicipio))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (pathRemessa == null) {
			if (other.pathRemessa != null)
				return false;
		} else if (!pathRemessa.equals(other.pathRemessa))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		return true;
	}
}
