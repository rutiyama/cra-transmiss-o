package br.com.softcenter.cra.model.beans;

public class BeanMensagem {
	private String datahora;
	private String codigo;
	private String ocorrencia;
	
	public String getDatahora() {
		return datahora;
	}
	public void setDatahora(String datahora) {
		this.datahora = datahora;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getOcorrencia() {
		return ocorrencia;
	}
	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}
	
	@Override
	public String toString() {
		return "BeanMensagem [datahora=" + datahora + ", codigo=" + codigo + ", ocorrencia=" + ocorrencia + "]";
	}
}
