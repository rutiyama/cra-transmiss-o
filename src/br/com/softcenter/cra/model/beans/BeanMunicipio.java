package br.com.softcenter.cra.model.beans;

import java.io.Serializable;
import java.util.Date;

public class BeanMunicipio implements Serializable{
	private int codigo;
	private String nome;
	private Date dataCadastro;
	private Character indAtivo;
	private Character tipoPagamento;
	private Character tipoEnvio;
	
	public BeanMunicipio(){}

	
	public BeanMunicipio(int codigo, String nome, Date dataCadastro,
			Character indAtivo, Character tipoPagamento, Character tipoEnvio) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.dataCadastro = dataCadastro;
		this.indAtivo = indAtivo;
		this.tipoPagamento = tipoPagamento;
		this.tipoEnvio = tipoEnvio;
	}


	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Character getIndAtivo() {
		return indAtivo;
	}

	public void setIndAtivo(Character indAtivo) {
		this.indAtivo = indAtivo;
	}

	public Character getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(Character tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public Character getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(Character tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	@Override
	public String toString() {
		return "BeanMunicipio [codigo=" + codigo + ", nome=" + nome
				+ ", dataCadastro=" + dataCadastro + ", indAtivo=" + indAtivo
				+ ", tipoPagamento=" + tipoPagamento + ", tipoEnvio="
				+ tipoEnvio + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		result = prime * result
				+ ((dataCadastro == null) ? 0 : dataCadastro.hashCode());
		result = prime * result
				+ ((indAtivo == null) ? 0 : indAtivo.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((tipoEnvio == null) ? 0 : tipoEnvio.hashCode());
		result = prime * result
				+ ((tipoPagamento == null) ? 0 : tipoPagamento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BeanMunicipio other = (BeanMunicipio) obj;
		if (codigo != other.codigo)
			return false;
		if (dataCadastro == null) {
			if (other.dataCadastro != null)
				return false;
		} else if (!dataCadastro.equals(other.dataCadastro))
			return false;
		if (indAtivo == null) {
			if (other.indAtivo != null)
				return false;
		} else if (!indAtivo.equals(other.indAtivo))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (tipoEnvio == null) {
			if (other.tipoEnvio != null)
				return false;
		} else if (!tipoEnvio.equals(other.tipoEnvio))
			return false;
		if (tipoPagamento == null) {
			if (other.tipoPagamento != null)
				return false;
		} else if (!tipoPagamento.equals(other.tipoPagamento))
			return false;
		return true;
	}
	
 	
}
