package br.com.softcenter.cra.model.beans;

/**
 * @author Rodrigo Junior Utiyama
 * @since  26-08-2015
 */


public class BeanPerfilMaster extends Perfil{

	public BeanPerfilMaster() {
		super.tipoPerfil = 'M';
	}
	
	@Override
	public Character getTipoPerfil() {
		return super.tipoPerfil;
	}
	
}
