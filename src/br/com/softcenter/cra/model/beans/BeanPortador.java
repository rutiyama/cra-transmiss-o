package br.com.softcenter.cra.model.beans;

import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since  30-09-2015
 */

public class BeanPortador {
	private Integer codigo;
	private Integer agencia;
	private String  nome;
	private String agenciaCentralizadora;
	private Date   dataCadastro;
	private String endereco;
	private boolean isAtivo;
	private String codigoMunicipio;
	private String cep;
	private String uf;
	private String cpnj;
	
	public BeanPortador(){
	}
	
	public BeanPortador(Integer codigo, Integer agencia, String nome,
			String agenciaCentralizadora, Date dataCadastro, String endereco,
			boolean isAtivo, String codigoMunicipio, String cep, String uf,
			String cpnj) {
		super();
		this.codigo = codigo;
		this.agencia = agencia;
		this.nome = nome;
		this.agenciaCentralizadora = agenciaCentralizadora;
		this.dataCadastro = dataCadastro;
		this.endereco = endereco;
		this.isAtivo = isAtivo;
		this.codigoMunicipio = codigoMunicipio;
		this.cep = cep;
		this.uf = uf;
		this.cpnj = cpnj;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getAgencia() {
		return agencia;
	}

	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getAgenciaCentralizadora() {
		return agenciaCentralizadora;
	}

	public void setAgenciaCentralizadora(String agenciaCentralizadora) {
		this.agenciaCentralizadora = agenciaCentralizadora;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public boolean isAtivo() {
		return isAtivo;
	}

	public void setAtivo(boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}

	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCpnj() {
		return cpnj;
	}

	public void setCpnj(String cpnj) {
		this.cpnj = cpnj;
	}

	@Override
	public String toString() {
		return "BeanPortador [codigo=" + codigo + ", agencia=" + agencia
				+ ", nome=" + nome + ", agenciaCentralizadora="
				+ agenciaCentralizadora + ", dataCadastro=" + dataCadastro
				+ ", endereco=" + endereco + ", isAtivo=" + isAtivo
				+ ", codigoMunicipio=" + codigoMunicipio + ", cep=" + cep
				+ ", uf=" + uf + ", cpnj=" + cpnj + "]";
	}
}
