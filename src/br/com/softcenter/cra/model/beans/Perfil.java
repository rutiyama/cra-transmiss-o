package br.com.softcenter.cra.model.beans;

import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since  26-08-2015
 */

public abstract class Perfil {
	protected int codigo;
	protected String descricao;
	protected Date dataCadastro;
	protected Boolean isAtivo;
	protected Character tipoPerfil;
	
	public abstract Character getTipoPerfil();

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Boolean getIsAtivo() {
		return isAtivo;
	}

	public void setIsAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	public void setTipoPerfil(Character tipoPerfil) {
		this.tipoPerfil = tipoPerfil;
	}

	@Override
	public String toString() {
		return "Perfil [codigo=" + codigo + ", descricao=" + descricao
				+ ", dataCadastro=" + dataCadastro + ", isAtivo=" + isAtivo
				+ ", tipoPerfil=" + tipoPerfil + "]";
	}
	
	
	
}
