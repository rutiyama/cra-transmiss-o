package br.com.softcenter.cra.model.beans.remessa;

import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since 02-10-2015
 */

public class BeanRemessaHeader {
	private String	idRegistro;
	private String	codigoPortador;
	private String	nomePortador;
	private String	dataMovimento;
	private String	idTransacaoRemetente;
	private String	idTransacaoDestinatario;
	private String	idTransacaoTipo;
	private String	numeroSequencialRemessa;
	private String	quantidadeRegistrosRemessa;
	private String	quantidadeTitulosRemessa;
	private String	quantidadeIndicacoesRemessa;
	private String	quantidadeOriginaisRemessa;
	private String	quantidadeIdentificacaoCentralizadora;
	private String	versaoLayout;
	private String	codigoMunicipioPracaPagamento;
	private String	complementadoRegistro;
	private String	numeroSequencialRegistroArquivo;
	
	
	
	public BeanRemessaHeader() {
	}
	
	public String getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}
	public String getCodigoPortador() {
		return codigoPortador;
	}
	public void setCodigoPortador(String codigoPortador) {
		this.codigoPortador = codigoPortador;
	}
	public String getNomePortador() {
		return nomePortador;
	}
	public void setNomePortador(String nomePortador) {
		this.nomePortador = nomePortador;
	}
	public String getDataMovimento() {
		return dataMovimento;
	}
	public void setDataMovimento(String dataMovimento) {
		this.dataMovimento = dataMovimento;
	}
	public String getIdTransacaoRemetente() {
		return idTransacaoRemetente;
	}
	public void setIdTransacaoRemetente(String idTransacaoRemetente) {
		this.idTransacaoRemetente = idTransacaoRemetente;
	}
	public String getIdTransacaoDestinatario() {
		return idTransacaoDestinatario;
	}
	public void setIdTransacaoDestinatario(String idTransacaoDestinatario) {
		this.idTransacaoDestinatario = idTransacaoDestinatario;
	}
	public String getIdTransacaoTipo() {
		return idTransacaoTipo;
	}
	public void setIdTransacaoTipo(String idTransacaoTipo) {
		this.idTransacaoTipo = idTransacaoTipo;
	}
	public String getNumeroSequencialRemessa() {
		return numeroSequencialRemessa;
	}
	public void setNumeroSequencialRemessa(String numeroSequencialRemessa) {
		this.numeroSequencialRemessa = numeroSequencialRemessa;
	}
	public String getQuantidadeRegistrosRemessa() {
		return quantidadeRegistrosRemessa;
	}
	public void setQuantidadeRegistrosRemessa(String quantidadeRegistrosRemessa) {
		this.quantidadeRegistrosRemessa = quantidadeRegistrosRemessa;
	}
	public String getQuantidadeTitulosRemessa() {
		return quantidadeTitulosRemessa;
	}
	public void setQuantidadeTitulosRemessa(String quantidadeTitulosRemessa) {
		this.quantidadeTitulosRemessa = quantidadeTitulosRemessa;
	}
	public String getQuantidadeIndicacoesRemessa() {
		return quantidadeIndicacoesRemessa;
	}
	public void setQuantidadeIndicacoesRemessa(String quantidadeIndicacoesRemessa) {
		this.quantidadeIndicacoesRemessa = quantidadeIndicacoesRemessa;
	}
	public String getQuantidadeOriginaisRemessa() {
		return quantidadeOriginaisRemessa;
	}
	public void setQuantidadeOriginaisRemessa(String quantidadeOriginaisRemessa) {
		this.quantidadeOriginaisRemessa = quantidadeOriginaisRemessa;
	}
	public String getQuantidadeIdentificacaoCentralizadora() {
		return quantidadeIdentificacaoCentralizadora;
	}
	public void setQuantidadeIdentificacaoCentralizadora(String quantidadeIdentificacaoCentralizadora) {
		this.quantidadeIdentificacaoCentralizadora = quantidadeIdentificacaoCentralizadora;
	}
	public String getVersaoLayout() {
		return versaoLayout;
	}
	public void setVersaoLayout(String versaoLayout) {
		this.versaoLayout = versaoLayout;
	}
	public String getCodigoMunicipioPracaPagamento() {
		return codigoMunicipioPracaPagamento;
	}
	public void setCodigoMunicipioPracaPagamento(String codigoMunicipioPracaPagamento) {
		this.codigoMunicipioPracaPagamento = codigoMunicipioPracaPagamento;
	}
	public String getComplementadoRegistro() {
		return complementadoRegistro;
	}
	public void setComplementadoRegistro(String complementadoRegistro) {
		this.complementadoRegistro = complementadoRegistro;
	}
	public String getNumeroSequencialRegistroArquivo() {
		return numeroSequencialRegistroArquivo;
	}
	public void setNumeroSequencialRegistroArquivo(String numeroSequencialRegistroArquivo) {
		this.numeroSequencialRegistroArquivo = numeroSequencialRegistroArquivo;
	}
	
	@Override
	public String toString() {
		return "BeanRemessaHeader [idRegistro=" + idRegistro + ", codigoPortador=" + codigoPortador + ", nomePortador="
				+ nomePortador + ", dataMovimento=" + dataMovimento + ", idTransacaoRemetente=" + idTransacaoRemetente
				+ ", idTransacaoDestinatario=" + idTransacaoDestinatario + ", idTransacaoTipo=" + idTransacaoTipo
				+ ", numeroSequencialRemessa=" + numeroSequencialRemessa + ", quantidadeRegistrosRemessa="
				+ quantidadeRegistrosRemessa + ", quantidadeTitulosRemessa=" + quantidadeTitulosRemessa
				+ ", quantidadeIndicacoesRemessa=" + quantidadeIndicacoesRemessa + ", quantidadeOriginaisRemessa="
				+ quantidadeOriginaisRemessa + ", quantidadeIdentificacaoCentralizadora="
				+ quantidadeIdentificacaoCentralizadora + ", versaoLayout=" + versaoLayout
				+ ", codigoMunicipioPracaPagamento=" + codigoMunicipioPracaPagamento + ", complementadoRegistro="
				+ complementadoRegistro + ", numeroSequencialRegistroArquivo=" + numeroSequencialRegistroArquivo + "]";
	}
}
