package br.com.softcenter.cra.model.beans.remessa;


/**
 * @author Rodrigo Junior Utiyama
 * @since 02-10-2015
 */
public class BeanRemessaTransacao {
	private String	identificaoRegistro;
	private String	codigoPortador;
	private String	declaracaoPortador;
	private String	complementoDoRegistro;
	private String	numeroSequencialRegistroNoArquivo;

	private BeanRemessaTransacaoBanco beanBanco;
	private BeanRemessaTransacaoCedente beanCedente;
	private BeanRemessaTransacaoDevedor beanDevedor;
	private BeanRemessaTransacaoSacadorVendedor beanSacadorDevedores;
	private BeanRemessaTransacaoTitulo beanTitulo;
	private BeanRemessaTransacaoRestrito beanRestrito;
	
	public BeanRemessaTransacao() {
		beanBanco 			 = new BeanRemessaTransacaoBanco();
		beanCedente 		 = new BeanRemessaTransacaoCedente();
		beanDevedor 		 = new BeanRemessaTransacaoDevedor();
		beanSacadorDevedores = new BeanRemessaTransacaoSacadorVendedor();
		beanTitulo 			 = new BeanRemessaTransacaoTitulo();
		beanRestrito		 = new BeanRemessaTransacaoRestrito();
	}

	public String getIdentificaoRegistro() {
		return identificaoRegistro;
	}

	public void setIdentificaoRegistro(String identificaoRegistro) {
		this.identificaoRegistro = identificaoRegistro;
	}

	public String getCodigoPortador() {
		return codigoPortador;
	}

	public void setCodigoPortador(String codigoPortador) {
		this.codigoPortador = codigoPortador;
	}

	public String getDeclaracaoPortador() {
		return declaracaoPortador;
	}

	public void setDeclaracaoPortador(String declaracaoPortador) {
		this.declaracaoPortador = declaracaoPortador;
	}

	public String getComplementoDoRegistro() {
		return complementoDoRegistro;
	}

	public void setComplementoDoRegistro(String complementoDoRegistro) {
		this.complementoDoRegistro = complementoDoRegistro;
	}

	public String getNumeroSequencialRegistroNoArquivo() {
		return numeroSequencialRegistroNoArquivo;
	}

	public void setNumeroSequencialRegistroNoArquivo(String numeroSequencialRegistroNoArquivo) {
		this.numeroSequencialRegistroNoArquivo = numeroSequencialRegistroNoArquivo;
	}

	public BeanRemessaTransacaoBanco getBeanBanco() {
		return beanBanco;
	}

	public void setBeanBanco(BeanRemessaTransacaoBanco beanBanco) {
		this.beanBanco = beanBanco;
	}

	public BeanRemessaTransacaoCedente getBeanCedente() {
		return beanCedente;
	}

	public void setBeanCedente(BeanRemessaTransacaoCedente beanCedente) {
		this.beanCedente = beanCedente;
	}

	public BeanRemessaTransacaoDevedor getBeanDevedor() {
		return beanDevedor;
	}

	public void setBeanDevedor(BeanRemessaTransacaoDevedor beanDevedor) {
		this.beanDevedor = beanDevedor;
	}

	public BeanRemessaTransacaoSacadorVendedor getBeanSacadorDevedores() {
		return beanSacadorDevedores;
	}

	public void setBeanSacadorDevedores(BeanRemessaTransacaoSacadorVendedor beanSacadorDevedores) {
		this.beanSacadorDevedores = beanSacadorDevedores;
	}

	public BeanRemessaTransacaoTitulo getBeanTitulo() {
		return beanTitulo;
	}

	public void setBeanTitulo(BeanRemessaTransacaoTitulo beanTitulo) {
		this.beanTitulo = beanTitulo;
	}

	public BeanRemessaTransacaoRestrito getBeanRestrito() {
		return beanRestrito;
	}

	public void setBeanRestrito(BeanRemessaTransacaoRestrito beanRestrito) {
		this.beanRestrito = beanRestrito;
	}

	@Override
	public String toString() {
		return "BeanRemessaTransacao [identificaoRegistro=" + identificaoRegistro + ", codigoPortador=" + codigoPortador
				+ ", declaracaoPortador=" + declaracaoPortador + ", complementoDoRegistro=" + complementoDoRegistro
				+ ", numeroSequencialRegistroNoArquivo=" + numeroSequencialRegistroNoArquivo + ", beanBanco="
				+ beanBanco + ", beanCedente=" + beanCedente + ", beanDevedor=" + beanDevedor
				+ ", beanSacadorDevedores=" + beanSacadorDevedores + ", beanTitulo=" + beanTitulo + ", beanRestrito="
				+ beanRestrito + "]";
	}
	
	
}
