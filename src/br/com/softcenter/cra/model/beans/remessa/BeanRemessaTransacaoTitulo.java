package br.com.softcenter.cra.model.beans.remessa;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since 02-10-2015
 */
public class BeanRemessaTransacaoTitulo {
	private String	nossoNumero;
	private String	especieTitulo;
	private String	numeroTitulo;
	private String	dataEmissaoTitulo;
	private String	dataVencimentoTitulo;
	private String	tipoMoeda;
	private String	valorTitulo;
	private String	saldoTitulo;
	private String	pracaProtesto;
	private String 	tipoEndosso;
	private String 	informacaoAceite;
	private String 	protestoPorMotivoFalencia;//
	private String 	instrumentoProtesto;//
	
	public BeanRemessaTransacaoTitulo() {
	}
	
	public String getNossoNumero() {
		return nossoNumero;
	}
	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}
	public String getEspecieTitulo() {
		return especieTitulo;
	}
	public void setEspecieTitulo(String especieTitulo) {
		this.especieTitulo = especieTitulo;
	}
	public String getNumeroTitulo() {
		return numeroTitulo;
	}
	public void setNumeroTitulo(String numeroTitulo) {
		this.numeroTitulo = numeroTitulo;
	}
	public String getDataEmissaoTitulo() {
		return dataEmissaoTitulo;
	}
	public void setDataEmissaoTitulo(String dataEmissaoTitulo) {
		this.dataEmissaoTitulo = dataEmissaoTitulo;
	}
	public String getDataVencimentoTitulo() {
		return dataVencimentoTitulo;
	}
	public void setDataVencimentoTitulo(String dataVencimentoTitulo) {
		this.dataVencimentoTitulo = dataVencimentoTitulo;
	}
	public String getTipoMoeda() {
		return tipoMoeda;
	}
	public void setTipoMoeda(String tipoMoeda) {
		this.tipoMoeda = tipoMoeda;
	}
	public String getValorTitulo() {
		return valorTitulo;
	}
	public void setValorTitulo(String valorTitulo) {
		this.valorTitulo = valorTitulo;
	}
	public String getSaldoTitulo() {
		return saldoTitulo;
	}
	public void setSaldoTitulo(String saldoTitulo) {
		this.saldoTitulo = saldoTitulo;
	}
	public String getPracaProtesto() {
		return pracaProtesto;
	}
	public void setPracaProtesto(String pracaProtesto) {
		this.pracaProtesto = pracaProtesto;
	}
	public String getTipoEndosso() {
		return tipoEndosso;
	}
	public void setTipoEndosso(String tipoEndosso) {
		this.tipoEndosso = tipoEndosso;
	}
	public String getInformacaoAceite() {
		return informacaoAceite;
	}
	public void setInformacaoAceite(String informacaoAceite) {
		this.informacaoAceite = informacaoAceite;
	}
	public String getProtestoPorMotivoFalencia() {
		return protestoPorMotivoFalencia;
	}
	public void setProtestoPorMotivoFalencia(String protestoPorMotivoFalencia) {
		this.protestoPorMotivoFalencia = protestoPorMotivoFalencia;
	}
	public String getInstrumentoProtesto() {
		return instrumentoProtesto;
	}
	public void setInstrumentoProtesto(String instrumentoProtesto) {
		this.instrumentoProtesto = instrumentoProtesto;
	}
	
	@Override
	public String toString() {
		return "BeanRemessaTransacaoTitulo [nossoNumero=" + nossoNumero + ", especieTitulo=" + especieTitulo
				+ ", numeroTitulo=" + numeroTitulo + ", dataEmissaoTitulo=" + dataEmissaoTitulo
				+ ", dataVencimentoTitulo=" + dataVencimentoTitulo + ", tipoMoeda=" + tipoMoeda + ", valorTitulo="
				+ valorTitulo + ", saldoTitulo=" + saldoTitulo + ", pracaProtesto=" + pracaProtesto + ", tipoEndosso="
				+ tipoEndosso + ", informacaoAceite=" + informacaoAceite + ", protestoPorMotivoFalencia="
				+ protestoPorMotivoFalencia + ", instrumentoProtesto=" + instrumentoProtesto + "]";
	}
	
	
	
}
