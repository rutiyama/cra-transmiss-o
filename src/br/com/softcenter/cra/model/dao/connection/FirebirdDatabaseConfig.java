/**
 * Classe respons�vel por realizar a configuração
 * da conex�o com o banco de dados 
 */
package br.com.softcenter.cra.model.dao.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import br.com.softcenter.cra.model.beans.BeanIniUtils;
import br.com.softcenter.cra.utils.arquivoini.PropertyFile;

/**
 * @author Rodrigo Junior Utiyama
 * @since  26-08-2015
 */


public class FirebirdDatabaseConfig {
	
	//Atributo para conectar ao banco de dados JDBC:
	private java.sql.Connection conn; 
	private static FirebirdDatabaseConfig configuracaoBancoDeDados;

	private FirebirdDatabaseConfig(){
		try{
			Class.forName("org.firebirdsql.jdbc.FBDriver");
			conn = DriverManager.getConnection(PropertyFile.getProperty().getConfig().getDatabase_url(),
											   PropertyFile.getProperty().getConfig().getDatabase_user(),
											   PropertyFile.getProperty().getConfig().getDatabase_pass());
			
			
		}catch(ClassNotFoundException cnfex){
			Logger.getLogger("br.com.softcenter.cra.model.dao.connection").error(cnfex.getMessage());
		}catch(SQLException sqlex){
			Logger.getLogger("br.com.softcenter.cra.model.dao.connection").error("br.com.softcenter.cra.model.dao.connection");
		} 
	}
 
	public static FirebirdDatabaseConfig getConfig(){
		if (configuracaoBancoDeDados == null)
			configuracaoBancoDeDados = new FirebirdDatabaseConfig();
		
		return configuracaoBancoDeDados;
	}
	
	public Connection getConnection(){
		return conn;
	}
	 
}
