package br.com.softcenter.cra.model.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.softcenter.cra.model.beans.BeanComarcasWS;
import br.com.softcenter.cra.model.dao.connection.FirebirdDatabaseConfig;

public class ComarcasWSDAOImpl implements DAOModel<BeanComarcasWS, Integer>{

	private PreparedStatement pstmt;
	private Connection conn;
	private Statement stmt;
	
	public ComarcasWSDAOImpl() {
		try{
			conn = FirebirdDatabaseConfig.getConfig().getConnection();
			stmt = conn.createStatement();
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
	}
	
	@Override
	public boolean insert(BeanComarcasWS usuario) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(BeanComarcasWS usuario) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(BeanComarcasWS usuario, Integer codigo) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BeanComarcasWS select(Integer codigo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BeanComarcasWS> select() {
//		String sql = "select * from COMARCAS_WS where senha is not null";
		String sql = "select * from COMARCAS_WS where comarcas_ws.senha is not null and comarcas_ws.patharqremessa != ''";
		try{
			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			List<BeanComarcasWS> comarcas = new ArrayList<BeanComarcasWS>();
			
			while(rs.next()){
				BeanComarcasWS comarca = new BeanComarcasWS();
				comarca.setCodigoMunicipio(rs.getString("CODIGOMUNICIPIO"));
				comarca.setLogin(rs.getString("LOGIN"));
				comarca.setSenha(rs.getString("SENHA"));
				comarca.setPathRemessa(rs.getString("PATHARQREMESSA"));
				comarcas.add(comarca);
			}
			
			return comarcas;
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		} finally{
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public int getLastRecordCount() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
