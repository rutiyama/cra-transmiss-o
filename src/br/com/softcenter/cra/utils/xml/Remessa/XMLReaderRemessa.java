package br.com.softcenter.cra.utils.xml.Remessa; 
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import br.com.softcenter.cra.controller.enums.RemessaEnum;
import br.com.softcenter.cra.controller.enums.RemessaEnumController;
import br.com.softcenter.cra.model.beans.BeanMensagem;
import br.com.softcenter.cra.model.beans.remessa.BeanRemessa;
import br.com.softcenter.cra.model.beans.remessa.BeanRemessaHeader;
import br.com.softcenter.cra.model.beans.remessa.BeanRemessaTransacao;
import br.com.softcenter.cra.utils.xml.CRAException;
import br.com.softcenter.cra.utils.xml.XMLReader; 

/**
 * @author Rodrigo Junior Utiyama
 * @since 24-10-2015
 */
public class XMLReaderRemessa extends DefaultHandler implements XMLReader{

	private String conteudo;
	private String tagAtual;
	private BeanRemessa remessa;
	private int codigoOcorrencia; 
	private boolean isErrorResult;
	private String primeiroElemento = null;
	private String valorElementoAtual;
	private BeanMensagem mensagemErro;
	
	private List<BeanRemessa> remessaList;
	
	private static final String PGF = "923";
	private static final String CREF = "919";
	private static final String BANCO_CENTRAL = "930";
	
	private Logger logger;
	
	public XMLReaderRemessa(String conteudo) {
		this.conteudo 	   = conteudo;
		this.remessaList   = new ArrayList<BeanRemessa>();
		this.isErrorResult = false; //reset 
		this.logger 	   = Logger.getLogger("br.com.softcenter.cra.utils.xml.Remessa");
	}
	
	@Override
	public void iniciarParsingXML() throws Exception{
		newSaxParserInstance().parse(new InputSource(new StringReader(conteudo)), this);
	}
	
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		tagAtual = qName;
	
		if(primeiroElemento == null){
			primeiroElemento = tagAtual;
			
			if(primeiroElemento.equals("relatorio")){
				mensagemErro = new BeanMensagem();
				isErrorResult = true;
			}
		}
		
		try{
			if(!isErrorResult){
				if(tagAtual.compareTo("hd") == 0){
					this.remessa  = new BeanRemessa();
					iniciarRemessa(atts);
				}else if(tagAtual.compareTo("tr") == 0)
					controlarItensRemessa(atts);
				else if(tagAtual.compareTo("tl") == 0)
					finalizarRemessa(atts);
			}
		}catch(CRAException craex){
			logger.error(craex.getMessage());
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		super.characters(ch, start, length);
		valorElementoAtual = new String(ch, start, length);
		
		if(isErrorResult)
			lerMensagemErro();
	}

	private SAXParser newSaxParserInstance() throws Exception{
		SAXParserFactory factory = SAXParserFactory.newInstance();
		return factory.newSAXParser();
	}
	
	private void iniciarRemessa(Attributes atts) throws CRAException{
		BeanRemessaHeader header = remessa.getHeader();
		header.setIdRegistro(atts.getValue("h01"));
		header.setCodigoPortador(atts.getValue("h02"));
		header.setNomePortador(atts.getValue("h03"));
		
		String portador = header.getCodigoPortador();
		
		if(portador.equals(PGF) || portador.equals(CREF) || portador.equals(BANCO_CENTRAL))
			throw new CRAException("Não foi possível transferir arquivos para o portador " + header.getNomePortador());
		
		header.setDataMovimento(atts.getValue("h04"));
		header.setIdTransacaoRemetente(atts.getValue("h05"));
		header.setIdTransacaoDestinatario(atts.getValue("h06"));
		header.setIdTransacaoTipo(atts.getValue("h07"));
		header.setNumeroSequencialRemessa(atts.getValue("h08")); 
		header.setQuantidadeRegistrosRemessa(atts.getValue("h09"));
		header.setQuantidadeTitulosRemessa(atts.getValue("h10"));
		header.setQuantidadeIndicacoesRemessa(atts.getValue("h11"));
		header.setQuantidadeOriginaisRemessa(atts.getValue("h12"));
		header.setQuantidadeIdentificacaoCentralizadora(atts.getValue("h13"));
		header.setVersaoLayout(atts.getValue("h14"));
		header.setCodigoMunicipioPracaPagamento(atts.getValue("h15"));
		header.setComplementadoRegistro(atts.getValue("h16"));
		header.setNumeroSequencialRegistroArquivo(atts.getValue("h17"));
	}
	
	private void controlarItensRemessa(Attributes atts){
		BeanRemessaTransacao transacao = new BeanRemessaTransacao();
		transacao.setIdentificaoRegistro(atts.getValue("t01"));
		transacao.setCodigoPortador(atts.getValue("t02"));
		
		transacao.getBeanCedente().setAgenciaOuCodigoCedente(atts.getValue("t03"));
		transacao.getBeanCedente().setNomeCedenteOuFavorecido(atts.getValue("t04"));
		
		transacao.getBeanSacadorDevedores().setNomeSacadorOuVendedor(atts.getValue("t05"));
		transacao.getBeanSacadorDevedores().setDocumentoSacador(atts.getValue("t06"));
		transacao.getBeanSacadorDevedores().setEnderecoSacadorOuVendedor(atts.getValue("t07"));
		transacao.getBeanSacadorDevedores().setCepSacadorOuVendedor(atts.getValue("t08"));
		transacao.getBeanSacadorDevedores().setCidadeSacadorOuVendedor(atts.getValue("t09"));
		transacao.getBeanSacadorDevedores().setUfSacadorOuVendedor(atts.getValue("t10"));
		
		transacao.getBeanTitulo().setNossoNumero(atts.getValue("t11"));
		transacao.getBeanTitulo().setEspecieTitulo(atts.getValue("t12"));
		transacao.getBeanTitulo().setNumeroTitulo(atts.getValue("t13"));
		transacao.getBeanTitulo().setDataEmissaoTitulo(atts.getValue("t14"));
		transacao.getBeanTitulo().setDataVencimentoTitulo(atts.getValue("t15"));
		transacao.getBeanTitulo().setTipoMoeda(atts.getValue("t16"));
		transacao.getBeanTitulo().setValorTitulo(atts.getValue("t17"));
		transacao.getBeanTitulo().setSaldoTitulo(atts.getValue("t18"));
		transacao.getBeanTitulo().setPracaProtesto(atts.getValue("t19"));
		transacao.getBeanTitulo().setTipoEndosso(atts.getValue("t20"));
		transacao.getBeanTitulo().setInformacaoAceite(atts.getValue("t21"));
		
		transacao.getBeanDevedor().setNumeroControleDevedor(atts.getValue("t22"));
		transacao.getBeanDevedor().setNomeDevedor(atts.getValue("t23"));
		transacao.getBeanDevedor().setTipoIdentificacaoDevedor(atts.getValue("t24"));
		transacao.getBeanDevedor().setNumeroIdentificacaoDevedor(atts.getValue("t25"));
		transacao.getBeanDevedor().setDocumentoDevedor(atts.getValue("t26"));
		transacao.getBeanDevedor().setEnderecoDevedor(atts.getValue("t27"));
		transacao.getBeanDevedor().setCepDevedor(atts.getValue("t28"));
		transacao.getBeanDevedor().setCidadeDevedor(atts.getValue("t29"));
		transacao.getBeanDevedor().setUfDevedor(atts.getValue("t30"));
		
		transacao.getBeanRestrito().setCodigoCartorio(atts.getValue("t31"));
		transacao.getBeanRestrito().setNumeroProtocoloCartorio(atts.getValue("t32"));
		transacao.getBeanRestrito().setTipoOcorrencia(atts.getValue("t33"));
		transacao.getBeanRestrito().setDataProtocolo(atts.getValue("t34"));
		transacao.getBeanRestrito().setValorCustasCartorio(atts.getValue("t35"));
		transacao.setDeclaracaoPortador(atts.getValue("t36"));
		transacao.getBeanRestrito().setDataOcorrencia(atts.getValue("t37"));
		transacao.getBeanRestrito().setCodigoIrregularidade(atts.getValue("t38"));
		transacao.getBeanDevedor().setBairroDevedor(atts.getValue("t39"));
		transacao.getBeanRestrito().setValorCustasCartorioDistribuidor(atts.getValue("t40"));
		transacao.getBeanRestrito().setRegistroDistruicao(atts.getValue("t41"));
		transacao.getBeanRestrito().setValorDemaisDespesas(atts.getValue("t42"));
		
		transacao.getBeanBanco().setNumeroOperacaoBanco(atts.getValue("t43"));
		transacao.getBeanBanco().setNumeroContratoBanco(atts.getValue("t44"));
		transacao.getBeanBanco().setNumeroParcelaDoContrato(atts.getValue("t45"));
		transacao.getBeanBanco().setTipoLetraCambio(atts.getValue("t46"));
		transacao.getBeanRestrito().setComplementoCodigoIrregularidade(atts.getValue("t47"));
		transacao.getBeanTitulo().setProtestoPorMotivoFalencia(atts.getValue("t48"));
		transacao.getBeanTitulo().setInstrumentoProtesto(atts.getValue("t49"));
		transacao.getBeanRestrito().setValorDemaisDespesasSedexCpmf(atts.getValue("t50"));
		transacao.setComplementoDoRegistro(atts.getValue("t51"));
		transacao.setNumeroSequencialRegistroNoArquivo(atts.getValue("t52"));
		
		remessa.getTransacoes().add(transacao);
	}
	
	private void finalizarRemessa(Attributes atts){
		remessa.getTrailler().setCodigoRegistro(atts.getValue("t01"));
		remessa.getTrailler().setCodigoPortador(atts.getValue("t02"));
		remessa.getTrailler().setNomePortador(atts.getValue("t03"));
		remessa.getTrailler().setDataMovimento(atts.getValue("t04"));
		remessa.getTrailler().setQuantidadeNaRemessa(atts.getValue("t05"));
		remessa.getTrailler().setValorRemessa(atts.getValue("t06"));
		remessa.getTrailler().setComplementoRegistro(atts.getValue("t07"));
		remessa.getTrailler().setSequenciaRegistroNoArquivo(atts.getValue("t08"));
		
		remessaList.add(remessa);
		//Verifica se mensagemErro é nulo, se sim, quer dizer que houve sucesso:
		if(mensagemErro == null)
			this.codigoOcorrencia = 200;
	}
	
	private void lerMensagemErro(){
		if(tagAtual.compareTo("datahora") == 0){
			mensagemErro.setDatahora(valorElementoAtual);
		}else if (tagAtual.compareTo("codigo") == 0){
			mensagemErro.setCodigo(valorElementoAtual);
			this.codigoOcorrencia = Integer.parseInt(valorElementoAtual);
		}else if (tagAtual.compareTo("ocorrencia") == 0)
			mensagemErro.setOcorrencia(valorElementoAtual);
	}
	
	public RemessaEnum getCodigoOcorrencia(){
		return RemessaEnumController.retornarOcorrencia(codigoOcorrencia);
	}

	public List<BeanRemessa> getRemessaList() {
		return remessaList;
	}
}
