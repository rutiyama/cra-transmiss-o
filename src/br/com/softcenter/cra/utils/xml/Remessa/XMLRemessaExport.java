package br.com.softcenter.cra.utils.xml.Remessa;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.softcenter.cra.model.beans.BeanComarcasWS;
import br.com.softcenter.cra.model.beans.remessa.BeanRemessa;
import br.com.softcenter.cra.model.beans.remessa.BeanRemessaTransacao;
import br.com.softcenter.cra.utils.date.DateUtils;
import br.com.softcenter.cra.utils.xml.CRAException;


/**
 * @author Rodrigo Junior Utiyama
 * @since 26-10-2015
 */
public class XMLRemessaExport {
	private List<BeanRemessa> remessas;
	private StringBuilder strBuilder;
	private BeanComarcasWS comarca;
	private Logger logger;
	
	public XMLRemessaExport(List<BeanRemessa> remessas) {
		this.remessas 	 = remessas;
		this.strBuilder  = new StringBuilder();
		this.logger      = Logger.getLogger("br.com.softcenter.cra.utils.xml.Remessa");
	}
	
	public void exportar(BeanComarcasWS comarca) throws CRAException, Exception{
		
		if(Files.notExists(Paths.get(comarca.getPathRemessa())))
			throw new CRAException("Diretório para a comarca: "+ comarca.getLogin()+" não existe! Verifique no sistema.");
		
		this.comarca = comarca;
		for(BeanRemessa remessa : remessas){
			//1: Se o arquivo ainda não foi baixado, é verificado pelo número de Sequencia da Remessa (número único):
			if (!XMLLogRemessa.ArquivoJaFoiBaixado(remessa.getHeader().getNumeroSequencialRemessa(), 
					comarca.getCodigoMunicipio(), remessa.getHeader().getCodigoPortador()))
				gravarRemessa(comarca, remessa);
			else
				logger.info("["+comarca.getLogin() +"] Arquivo já transferido.");
		}
	}

	private void gravarRemessa(BeanComarcasWS comarca, BeanRemessa remessa) throws IOException{
		String arquivoFinal = montarNomeArquivo(remessa);
		String URL = comarca.getPathRemessa()+arquivoFinal;
		
		logger.info("Salvando Arquivo: " + arquivoFinal + ". Comarca: " + comarca.getLogin() + ".." +  remessa.getHeader().getCodigoPortador());
		
		/* Pode ser que alguém baixou o arquivo manualmente. Portanto, faz essa pequena verificação. 
		   Senão, o conteúdo do arquivo será duplicado: */
		if(Files.exists(Paths.get(URL))){
			logger.info("Arquivo " + arquivoFinal + "já foi transferido manualmente. Sobrepondo o arquivo...");
			Files.delete(Paths.get(URL));
		}
		
		Files.write(
				Paths.get(URL), 
				montarHeader(remessa), 
				StandardOpenOption.CREATE);		

		Files.write(
				Paths.get(URL), 
				montarTransacoes(remessa), 
				StandardOpenOption.APPEND);		
		
		Files.write(
				Paths.get(URL), 
				montarTrailler(remessa), 
				StandardOpenOption.APPEND);		
		

		XMLLogRemessa.GravarLOG(arquivoFinal,remessa.getHeader().getNumeroSequencialRemessa(), comarca, remessa.getHeader().getCodigoPortador());
	}
	
	private byte[] montarHeader(BeanRemessa remessa){
		strBuilder.setLength(0); //Limpa o StringBuilder
		strBuilder.append(remessa.getHeader().getIdRegistro());
		strBuilder.append(remessa.getHeader().getCodigoPortador());
		strBuilder.append(strFixo(remessa.getHeader().getNomePortador(), 40));
		strBuilder.append(strFixo(remessa.getHeader().getDataMovimento(), 8));
		strBuilder.append(strFixo(remessa.getHeader().getIdTransacaoRemetente(), 3));
		strBuilder.append(strFixo(remessa.getHeader().getIdTransacaoDestinatario(), 3));
		strBuilder.append(strFixo(remessa.getHeader().getIdTransacaoTipo(), 3));
		strBuilder.append(strFixo(remessa.getHeader().getNumeroSequencialRemessa(), 6));
		strBuilder.append(strFixo(remessa.getHeader().getQuantidadeRegistrosRemessa(), 4));
		strBuilder.append(strFixo(remessa.getHeader().getQuantidadeTitulosRemessa(), 4));
		strBuilder.append(strFixo(remessa.getHeader().getQuantidadeIndicacoesRemessa(), 4));
		strBuilder.append(strFixo(remessa.getHeader().getQuantidadeOriginaisRemessa(), 4));
		strBuilder.append(strFixo(remessa.getHeader().getQuantidadeIdentificacaoCentralizadora(), 6));
		strBuilder.append(strFixo(remessa.getHeader().getVersaoLayout(), 3));
		strBuilder.append(strFixo(remessa.getHeader().getCodigoMunicipioPracaPagamento(), 7));
		strBuilder.append(strFixo("", 497));
		strBuilder.append(strFixo(remessa.getHeader().getNumeroSequencialRegistroArquivo(), 4));
		strBuilder.append(System.getProperty("line.separator"));
		
		return strBuilder.toString().getBytes();
	}
	
	private byte[] montarTransacoes(BeanRemessa remessa){
		strBuilder.setLength(0);
		
		for (BeanRemessaTransacao tr : remessa.getTransacoes()){
			strBuilder.append(tr.getIdentificaoRegistro());
			strBuilder.append(strFixo(tr.getCodigoPortador(), 3));
			strBuilder.append(strFixo(tr.getBeanCedente().getAgenciaOuCodigoCedente(), 15));
			strBuilder.append(strFixo(tr.getBeanCedente().getNomeCedenteOuFavorecido(), 45));
			strBuilder.append(strFixo(tr.getBeanSacadorDevedores().getNomeSacadorOuVendedor(), 45));
			strBuilder.append(strFixo(tr.getBeanSacadorDevedores().getDocumentoSacador(), 14));
			strBuilder.append(strFixo(tr.getBeanSacadorDevedores().getEnderecoSacadorOuVendedor(), 45));
			strBuilder.append(strFixo(tr.getBeanSacadorDevedores().getCepSacadorOuVendedor(), 8));
			strBuilder.append(strFixo(tr.getBeanSacadorDevedores().getCidadeSacadorOuVendedor(), 20));
			strBuilder.append(strFixo(tr.getBeanSacadorDevedores().getUfSacadorOuVendedor(),2));
			strBuilder.append(strFixo(tr.getBeanTitulo().getNossoNumero(), 15));
			strBuilder.append(strFixo(tr.getBeanTitulo().getEspecieTitulo(), 3));
			strBuilder.append(strFixo(tr.getBeanTitulo().getNumeroTitulo(), 11));
			strBuilder.append(strFixo(tr.getBeanTitulo().getDataEmissaoTitulo(), 8));
			strBuilder.append(strFixo(tr.getBeanTitulo().getDataVencimentoTitulo(), 8));
			strBuilder.append(strFixo(tr.getBeanTitulo().getTipoMoeda(), 3));
			strBuilder.append(strFixo(tr.getBeanTitulo().getValorTitulo(), 14));
			strBuilder.append(strFixo(tr.getBeanTitulo().getSaldoTitulo(), 14));
			strBuilder.append(strFixo(tr.getBeanTitulo().getPracaProtesto(), 20));
			strBuilder.append(strFixo(tr.getBeanTitulo().getTipoEndosso(), 1));
			strBuilder.append(strFixo(tr.getBeanTitulo().getInformacaoAceite(), 1));
			strBuilder.append(strFixo(tr.getBeanDevedor().getNumeroControleDevedor(), 1));
			strBuilder.append(strFixo(tr.getBeanDevedor().getNomeDevedor(), 45));
			strBuilder.append(strFixo(tr.getBeanDevedor().getTipoIdentificacaoDevedor(),3));
			strBuilder.append(strFixo(tr.getBeanDevedor().getNumeroIdentificacaoDevedor(), 14));
			strBuilder.append(strFixo(tr.getBeanDevedor().getDocumentoDevedor(), 11));
			strBuilder.append(strFixo(tr.getBeanDevedor().getEnderecoDevedor(),45));
			strBuilder.append(strFixo(tr.getBeanDevedor().getCepDevedor(), 8));
			strBuilder.append(strFixo(tr.getBeanDevedor().getCidadeDevedor(), 20));
			strBuilder.append(strFixo(tr.getBeanDevedor().getUfDevedor(), 2));
			strBuilder.append(strFixo(tr.getBeanRestrito().getCodigoCartorio(), 2));
			strBuilder.append(strFixo(tr.getBeanRestrito().getNumeroProtocoloCartorio(), 10));
			strBuilder.append(strFixo(tr.getBeanRestrito().getTipoOcorrencia(), 1));
			strBuilder.append(strFixo(tr.getBeanRestrito().getDataOcorrencia(), 8));
			strBuilder.append(strFixo(tr.getBeanRestrito().getValorCustasCartorio(), 10));
			strBuilder.append(strFixo(tr.getDeclaracaoPortador(), 1));
			strBuilder.append(strFixo(tr.getBeanRestrito().getDataOcorrencia(), 8));
			strBuilder.append(strFixo(tr.getBeanRestrito().getCodigoIrregularidade(),2));
			strBuilder.append(strFixo(tr.getBeanDevedor().getBairroDevedor(), 20));
			strBuilder.append(strFixo(tr.getBeanRestrito().getValorCustasCartorioDistribuidor(), 10));
			strBuilder.append(strFixo(tr.getBeanRestrito().getRegistroDistruicao(), 6));
			strBuilder.append(strFixo(tr.getBeanRestrito().getValorDemaisDespesas(), 10));
			strBuilder.append(strFixo(tr.getBeanBanco().getNumeroOperacaoBanco(), 5));
			strBuilder.append(strFixo(tr.getBeanBanco().getNumeroContratoBanco(), 15));
			strBuilder.append(strFixo(tr.getBeanBanco().getNumeroParcelaDoContrato(), 3));
			strBuilder.append(strFixo(tr.getBeanBanco().getTipoLetraCambio(), 1));
			strBuilder.append(strFixo(tr.getBeanRestrito().getComplementoCodigoIrregularidade(), 8));
			strBuilder.append(strFixo(tr.getBeanTitulo().getProtestoPorMotivoFalencia(), 1));
			strBuilder.append(strFixo(tr.getBeanTitulo().getInstrumentoProtesto(), 1));
			strBuilder.append(strFixo(tr.getBeanRestrito().getValorDemaisDespesasSedexCpmf(), 10));
			strBuilder.append(strFixo("", 19));
			strBuilder.append(strFixo(tr.getNumeroSequencialRegistroNoArquivo(),4));
			strBuilder.append(System.getProperty("line.separator"));
		}
		
		return strBuilder.toString().getBytes();
	}
	
	private byte[] montarTrailler(BeanRemessa remessa){
		strBuilder.setLength(0);
		strBuilder.append(strFixo(remessa.getTrailler().getCodigoRegistro(), 1));
		strBuilder.append(strFixo(remessa.getTrailler().getCodigoPortador(), 3));
		strBuilder.append(strFixo(remessa.getTrailler().getNomePortador(), 40));
		strBuilder.append(strFixo(remessa.getTrailler().getDataMovimento(), 8));
		strBuilder.append(strFixo(remessa.getTrailler().getQuantidadeNaRemessa(),5));
		strBuilder.append(strFixo(remessa.getTrailler().getValorRemessa(),18));
		strBuilder.append(strFixo("",521));
		strBuilder.append(strFixo(remessa.getTrailler().getSequenciaRegistroNoArquivo(), 4));
		
		return strBuilder.toString().getBytes();
	}
	
	//Gera um tamanho fixo de caracteres com espaços em branco para preencher:
	private  String strFixo(String string, int length) {
	    return String.format("%-"+length+ "s", string);
	}
	
	private String montarNomeArquivo(BeanRemessa remessa) throws IOException{
		String arquivo =  "B" + remessa.getHeader().getCodigoPortador() + 
				DateUtils.ConvertDateUtilsToCRADate(DateUtils.ConvertStringToCRADate(remessa.getHeader().getDataMovimento()));
		String sequencia = String.valueOf(XMLLogRemessa.RetornarSequencia(arquivo, comarca.getCodigoMunicipio(), remessa.getHeader().getCodigoPortador()));
		
		return arquivo+sequencia;  
	}
}
